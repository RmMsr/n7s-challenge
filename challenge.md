Write a script that...
1. Clones a git repository
2. Builds a docker image using the docker file in the repo
3. If the commit that is cloned/pulled has a tag, then tag the docker image with the same tag. If not, tag the image "latest".
4. Pushes the image to dockerhub.
5. Generates a kubernetes deployment file.
6. deploys/updates the deployment.

Challenge parameters
- For the challenge you can just print out the commands instead of running them.
- You can use https://github.com/datawire/hello-world as your project
- You can use any scripting language you prefer
- The script needs to be intended to run automatically and used on multiple repositories.
- This script should be able to be run again after initialization
- You can assume that kubernetes is already configured
