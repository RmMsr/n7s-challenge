#!/usr/bin/sh

# Prerequisit tools: git, docker, kubectl

# Assumptions:
# * The Dockerfile is in the root of the source repo.
# * A kubectl context including namespace is alreday present.
# * The IMAGE_REPO is a docker compatible container registry. Login
#   credentials are needed.

# Limitations:
# * The container registry credentials might not be stored securely.
# * Deployments without a unique and idempotent REFERENCE can not ensure
#   that the correct image is running in Kubernetes. Do not re-use tags.

set -e

help() {
    echo Usage:
    echo "  $0 [help]"
    echo
    echo Environmental variables:
    echo "  CHECKOUT_DIR    Optional local working dir. If not set a temporary"
    echo "                  folder is used."
    echo "  DEPLOYMENT      Name of the Kubernetes deployment to roll out."
    echo "  IMAGE_REPO      The location to push the container image to. Can"
    echo "                  be registry URL or Docker Hub repository"
    echo "                  (format: username/reponame)"
    echo "  REFERENCE       Git branch or tag to checkout. Defaults means"
    echo "                  master branch and image tag latest."
    echo "  SOURCE_REPO     Location of the source git repo."
    echo
    exit 1
}

cleanup() {
    if [ -z "$CHECKOUT_DIR" ]; then
        echo Removing "$work_dir"
        rm -rf "$work_dir"
    fi
}

get_source() {
    if git -C "$work_dir" status >/dev/null 2>&1; then
        echo Checking out "$reference" in "$work_dir"
        git -C "$work_dir" fetch --tags
        git -C "$work_dir" checkout "$reference"
	else
        repo_url="${SOURCE_REPO:?Missing SOURCE_REPO environmental variable}"
        echo Cloning "$reference" of "$repo_url" into "$work_dir":
        git clone --branch "$reference" --single-branch "$repo_url" "$work_dir"
    fi
}

build_container() {
    echo Building container image "$image_name:$image_tag"
    docker image build --tag "$image_name:$image_tag" "$work_dir"
}

push_container() {
    echo Uploading container image as "$image_name:$image_tag"
    docker login
    docker image push "$image_name:$image_tag"
}

deploy() {
    manifest="$work_dir/deployment.yaml"
    echo Generating Deployment manifest "$manifest"
    kubectl create deployment "$deployment_name" \
        --image="$image_name:$reference" \
        --dry-run=client \
        --save-config=false \
        --output=yaml \
        > "$manifest"
    echo Rolling out:
    cat "$manifest"
    kubectl apply --filename="$manifest"
}

if [ "$1" = help ]; then help; fi
trap cleanup EXIT

work_dir="${CHECKOUT_DIR:-$(mktemp --directory)}"
reference="${REFERENCE:-master}"
get_source

image_name="${IMAGE_REPO:?Missing environmental variable IMAGE_REPO}"
image_tag="$(echo "${REFERENCE:-latest}" | tr :/ _)"
build_container
push_container

deployment_name="${DEPLOYMENT:?Missing environmental variables DEPLOYMENT}"
deploy

echo Done
