## Deployment automation

Build and deploy a containerized application.

## Usage

The shell script [deploy.sh](./deploy.sh) expects a number of environmental
variables as parameters.

    Usage:
      ./deploy.sh [help]

    Environmental variables:
      CHECKOUT_DIR    Optional local working dir. If not set a temporary
                      folder is used.
      DEPLOYMENT      Name of the Kubernetes deployment to roll out.
      IMAGE_REPO      The location to push the container image to. Can
                      be registry URL or Docker Hub repository
                      (format: username/reponame)
      REFERENCE       Git branch or tag to checkout. Defaults means
                      master branch and image tag latest.
      SOURCE_REPO     Location of the source git repo.

## Example

    DEPLOYMENT=hello-app \
    IMAGE_REPO=rmmsr/n7s-challenge \
    REFERENCE=rhs/test \
    SOURCE_REPO=https://github.com/datawire/hello-world.git \
    ./deploy.sh

## License

This software is made available under the terms of the
[Unlicense](https://unlicense.org/): Use it in any way you like.

